<?php
	require_once 'config.php';

	if(isset($_POST['email'])){
		$email = addslashes($_POST['email']);
		$senha = md5(addslashes($_POST['senha']));


		$sql ="SELECT * FROM usuarios WHERE email = '$email' and senha = '$senha'";
		$sql = $con->query($sql);
		
		if($sql->rowCount() > 0) {//Verifica se possui registro
			$usuario = $sql->fetch();//Pega o primeiro usuário e armazena na variável usuário

			if($usuario['privilegio'] == "administrador") {//Verifica o nível de acesso do usuário logado
				header("Location: admin.php");
			}
			//Implementar as outras páginas dos níveis de acesso aqui...
		}else {
			header("Location: index.php");// Redireciona o usuário com email ou senha invalidos!";
		}
	}	
?>

<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Projeto Engenharia de Software2</title>
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>

<body>
	<div class="loginbox">
		<img src="assets/img/avatar.png" class="avatar">

		<h1>Login</h1>

		<form method="post">
			<label for="email">Usuário</label>
			<input type="email" name="email" id="email" required>

			<label for="senha">Senha</label>
			<input type="password" name="senha" id="senha" required>

			<input type="submit" value="Login">

			<a href="#">Esqueceu a senha?</a><br>			
		</form>
	</div>
</body>
</html>